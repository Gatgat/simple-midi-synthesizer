/*  Copyright © 2020-2021
            Gaëtan CHAMPARNAUD

    This file is part of SMS.

    SMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMS.  If not, see <https://www.gnu.org/licenses/>.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/fundamentals/scoped.hpp>

struct GLFWwindow;

class WindowContext final {
  public:
    WindowContext(u32 const width, u32 const height, char const * const title);
    ~WindowContext();

    auto should_close() const -> bool;
    auto begin_frame() -> void;
    auto end_frame() -> void;

    auto get_window() -> GLFWwindow & { return **window; }

  private:
    nlc::scoped<GLFWwindow *> window;
};
