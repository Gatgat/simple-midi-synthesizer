/*  Copyright © 2020-2021
            Gaëtan CHAMPARNAUD

    This file is part of SMS.

    SMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMS.  If not, see <https://www.gnu.org/licenses/>.*/

#pragma once

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/function.hpp>

enum class DeviceConnexionEvent { no_event, device_connected, device_disconnected };

struct PresetId final {
    int bank_id;
    int preset_id;
};

struct Preset final {
    nlc::string_view name;
    int id;
};

struct PresetView final {
    nlc::string_view preset_name;
    PresetId id;
};

struct Bank final {
    nlc::vector<Preset> presets;
    int id;
};

struct MidiViewer final {
    nlc::function<bool()> has_connected_device;
    nlc::function<PresetView()> get_current_preset;
    nlc::function<nlc::span<Bank const>()> get_preset_banks;
};

struct MidiController final {
    nlc::function<bool(void)> reconnect_device;
    nlc::function<bool(PresetId)> select_preset;
};
