/*  Copyright © 2020-2021
            Gaëtan CHAMPARNAUD

    This file is part of SMS.

    SMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMS.  If not, see <https://www.gnu.org/licenses/>.*/

#pragma once

#include <fluidsynth.h>

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/pointer.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/scoped.hpp>
#include <nlc/fundamentals/time.hpp>

#include "controller.hpp"

template<typename T> using fluid_obj = nlc::scoped<nlc::pointer<T>>;

class FluidContext final {
  public:
    FluidContext();

    auto update() -> DeviceConnexionEvent;
    auto get_midi_viewer() const -> MidiViewer const & { return viewer; }
    auto get_midi_controller() const -> MidiController const & { return controller; }

  private:
    nlc::standard_allocator allocator;
    fluid_obj<fluid_settings_t> settings;
    fluid_obj<fluid_synth_t> synthesizer;
    fluid_obj<fluid_audio_driver_t> audio_driver;
    fluid_obj<fluid_sequencer_t> sequencer;
    nlc::optional<fluid_obj<fluid_midi_driver_t>> midi_driver;  // Can be null if no input device
                                                                // are connected

    nlc::scoped<int> sound_font_id;
    nlc::scoped<fluid_seq_id_t> sequencer_client_id;
    nlc::time::duration last_connexion_try_time { nlc::time::now() };

    nlc::vector<Bank> preset_banks;
    MidiViewer viewer;
    MidiController controller;

    auto load_sound_font() -> nlc::scoped<int>;
    auto register_sequencer_client() -> nlc::scoped<fluid_seq_id_t>;
    auto fetch_preset_banks() -> nlc::vector<Bank>;
    auto create_viewer() -> MidiViewer;
    auto create_controller() -> MidiController;
    auto reconnect_device() -> bool;
};
