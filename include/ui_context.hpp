/*  Copyright © 2020-2021
            Gaëtan CHAMPARNAUD

    This file is part of SMS.

    SMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMS.  If not, see <https://www.gnu.org/licenses/>.*/

#pragma once

#include <nlc/dialect/optional.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/time.hpp>
#include <nlc/meta/numeric_limits.hpp>

#include "controller.hpp"

struct GLFWwindow;

struct FooterNote final {
    enum class Type { Info, Error };

    nlc::string message;
    Type type;
};

class UIContext final {
  public:
    UIContext(GLFWwindow & window);
    ~UIContext();

    auto update(MidiViewer const & viewer,
                MidiController const & controller,
                DeviceConnexionEvent const device_connexion_event) -> void;

  private:
    auto get_device_connexion_event_message(DeviceConnexionEvent const device_connexion_event)
        -> nlc::optional<FooterNote>;
    auto set_footer_message(FooterNote message) -> void;

  private:
    static constexpr auto never { nlc::meta::limits<nlc::time::duration::underlying>::max };
    nlc::standard_allocator allocator;
    FooterNote footer_msg {
        .message = { allocator, nullptr, 0u },
        .type = FooterNote::Type::Info,
    };
    nlc::time::duration footer_msg_hide_time { never };
};
