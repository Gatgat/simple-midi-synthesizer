/*  Copyright © 2020-2021
            Gaëtan CHAMPARNAUD

    This file is part of SMS.

    SMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMS.  If not, see <https://www.gnu.org/licenses/>.*/

#include <chrono>
#include <thread>
#include <stdlib.h>

#include <nlc/dialect/defer.hpp>

#include "fluid_context.hpp"
#include "ui_context.hpp"
#include "window_context.hpp"

int main(void) {
    FluidContext fluid_ctx;
    WindowContext window_ctx { 640, 480, "Simple Midi Synthesizer" };
    UIContext ui_ctx { window_ctx.get_window() };

    while (window_ctx.should_close() == false) {
        window_ctx.begin_frame();
        defer { window_ctx.end_frame(); };

        auto const device_connexion_event { fluid_ctx.update() };  // To try & connect to a device
                                                                   // if needed
        ui_ctx.update(fluid_ctx.get_midi_viewer(),
                      fluid_ctx.get_midi_controller(),
                      device_connexion_event);
    }

    return EXIT_SUCCESS;
}
