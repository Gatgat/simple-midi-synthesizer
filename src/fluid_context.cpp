/*  Copyright © 2020-2021
            Gaëtan CHAMPARNAUD

    This file is part of SMS.

    SMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMS.  If not, see <https://www.gnu.org/licenses/>.*/

#include "fluid_context.hpp"

#include <nlc/algo/find.hpp>
#include <nlc/algo/sort.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/dialect/string_iterator.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/dialect/utf8.hpp>
#include <nlc/fundamentals/string_manip.hpp>

#include "controller.hpp"

template<typename T> static auto fluid_arg(fluid_obj<T> const & obj) -> T * { return &**obj; }
template<typename T> static auto fluid_arg(nlc::pointer<T> const & ptr) -> T * { return &*ptr; }

static auto try_init_midi_driver(fluid_settings_t & settings, fluid_sequencer_t & sequencer)
    -> fluid_midi_driver_t * {
    return new_fluid_midi_driver(&settings,
                                 fluid_sequencer_add_midi_event_to_buffer,
                                 reinterpret_cast<void *>(&sequencer));
}

static auto try_create_midi_driver(fluid_settings_t & settings, fluid_sequencer_t & sequencer)
    -> nlc::optional<fluid_obj<fluid_midi_driver_t>> {
    auto * const driver = try_init_midi_driver(settings, sequencer);
    if (driver == nullptr)
        return nlc::null;

    return fluid_obj<fluid_midi_driver_t> { driver, [](auto & ptr) {
                                               delete_fluid_midi_driver(fluid_arg(ptr));
                                           } };
}

FluidContext::FluidContext()
    : settings { new_fluid_settings(), [](auto & ptr) { delete_fluid_settings(fluid_arg(ptr)); } }
    , synthesizer { new_fluid_synth(fluid_arg(settings)),
                    [](auto & ptr) { delete_fluid_synth(fluid_arg(ptr)); } }
    , audio_driver { new_fluid_audio_driver(fluid_arg(settings), fluid_arg(synthesizer)),
                     [](auto & ptr) { delete_fluid_audio_driver(fluid_arg(ptr)); } }
    , sequencer { new_fluid_sequencer2(false),
                  [](auto & ptr) { delete_fluid_sequencer(fluid_arg(ptr)); } }
    , midi_driver { try_create_midi_driver(**settings, **sequencer) }
    , sound_font_id { load_sound_font() }
    , sequencer_client_id { register_sequencer_client() }
    , preset_banks { fetch_preset_banks() }
    , viewer { create_viewer() }
    , controller { create_controller() } {}

auto FluidContext::update() -> DeviceConnexionEvent {
    constexpr auto connexion_try_delay { 1_s };

    auto const now { nlc::time::now() };
    if (now < last_connexion_try_time + connexion_try_delay)
        return DeviceConnexionEvent::no_event;

    last_connexion_try_time = now;

    if (midi_driver == nlc::null) {
        midi_driver = try_create_midi_driver(**settings, **sequencer);
        return (midi_driver != nlc::null ? DeviceConnexionEvent::device_connected
                                         : DeviceConnexionEvent::no_event);
    } else {
        return reconnect_device() ? DeviceConnexionEvent::no_event
                                  : DeviceConnexionEvent::device_disconnected;
    }
}

auto FluidContext::load_sound_font() -> nlc::scoped<int> {
    constexpr char const sound_font_file[] = { "assets/32MbGMStereo.sf2" };

    nlc_assert_msg(fluid_is_soundfont(sound_font_file),
                   "load_sound_font: '",
                   sound_font_file,
                   "' is not a sound font");

    auto const sound_file_id = fluid_synth_sfload(fluid_arg(synthesizer), sound_font_file, true);
    nlc_assert_msg(sound_file_id != -1, "load_sound_font: fluid_synth_sfload failed");

    return { sound_file_id,
             [this](auto const & id) { fluid_synth_sfunload(fluid_arg(synthesizer), id, false); } };
}

auto FluidContext::register_sequencer_client() -> nlc::scoped<fluid_seq_id_t> {
    auto const client_id =
        fluid_sequencer_register_fluidsynth(fluid_arg(sequencer), fluid_arg(synthesizer));
    nlc_assert_msg(client_id != FLUID_FAILED,
                   "register_sequencer_client: fluid_sequencer_register_fluidsynth failed");

    return { client_id, [this](auto const & id) {
                fluid_sequencer_unregister_client(fluid_arg(sequencer), id);
            } };
}

auto FluidContext::fetch_preset_banks() -> nlc::vector<Bank> {
    nlc::vector<Bank> res { allocator };

    nlc::pointer<fluid_sfont_t> const sound_font { fluid_synth_get_sfont_by_id(fluid_arg(synthesizer),
                                                                               *sound_font_id) };

    fluid_sfont_iteration_start(fluid_arg(sound_font));
    for (auto * preset = fluid_sfont_iteration_next(fluid_arg(sound_font)); preset != nullptr;
         preset = fluid_sfont_iteration_next(fluid_arg(sound_font))) {
        auto const bank_id { fluid_preset_get_banknum(preset) };

        auto & bank = [this, &res, bank_id]() -> Bank & {
            auto * const existing_bank =
                nlc::find_if(nlc::make_span(res),
                             [bank_id](auto const & item) { return bank_id == item.id; });

            if (existing_bank != nullptr)
                return *existing_bank;

            return res.append(nlc::vector<Preset> { allocator }, bank_id);
        }();

        bank.presets.append(nlc::string_view::from_c_str(fluid_preset_get_name(preset)),
                            fluid_preset_get_num(preset));
    }

    // Sort presets by id
    for (auto & bank : res) {
        nlc::sort<true>(nlc::make_span(bank.presets),
                        [](auto const & lhs, auto const & rhs) { return lhs.id < rhs.id; });
    }

    return res;
}

auto FluidContext::create_viewer() -> MidiViewer {
    return { .has_connected_device = { allocator, [this] { return midi_driver != nlc::null; } },
             .get_current_preset = { allocator,
                                     [this] {
                                         constexpr int channel { 0 };
                                         constexpr PresetView null_preset {
                                             .preset_name = { nullptr, 0u },
                                             .id = { .bank_id = -1, .preset_id = -1 }
                                         };

                                         int sfont_id, bank_id, preset_id;
                                         auto const res =
                                             fluid_synth_get_program(fluid_arg(synthesizer),
                                                                     channel,
                                                                     &sfont_id,
                                                                     &bank_id,
                                                                     &preset_id);

                                         if (res != FLUID_OK)
                                             return null_preset;

                                         auto * const sound_font =
                                             fluid_synth_get_sfont_by_id(fluid_arg(synthesizer),
                                                                         sfont_id);
                                         if (sound_font == nullptr)
                                             return null_preset;

                                         auto * const preset =
                                             fluid_sfont_get_preset(sound_font, bank_id, preset_id);
                                         if (preset == nullptr)
                                             return null_preset;

                                         auto const * const name = fluid_preset_get_name(preset);
                                         if (name == nullptr)
                                             return null_preset;

                                         return PresetView {
                                             .preset_name = nlc::string_view::from_c_str(name),
                                             .id = { .bank_id = bank_id, .preset_id = preset_id }
                                         };
                                     } },
             .get_preset_banks = { allocator, [this] { return nlc::make_span(preset_banks); } } };
}

auto FluidContext::create_controller() -> MidiController {
    return { .reconnect_device = { allocator, [this] { return reconnect_device(); } },
             .select_preset = { allocator, [this](PresetId const id) {
                                   auto const channel_count { fluid_synth_count_midi_channels(
                                       fluid_arg(synthesizer)) };

                                   // Select the preset for all the channels
                                   bool success { true };
                                   for (auto i : nlc::range(0, channel_count)) {
                                       if (fluid_synth_program_select(fluid_arg(synthesizer),
                                                                      i,
                                                                      *sound_font_id,
                                                                      id.bank_id,
                                                                      id.preset_id) != FLUID_OK) {
                                           nlc::warn("Couldn't set preset on channel ", i);
                                           success = false;
                                           // Don't break to try & select the preset on every
                                           // possible channel
                                       }
                                   }
                                   return success;
                               } } };
}

auto FluidContext::reconnect_device() -> bool {
    midi_driver = nlc::null;
    midi_driver = try_create_midi_driver(**settings, **sequencer);
    return midi_driver != nlc::null;
}
