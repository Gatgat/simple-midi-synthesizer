/*  Copyright © 2020-2021
            Gaëtan CHAMPARNAUD

    This file is part of SMS.

    SMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SMS.  If not, see <https://www.gnu.org/licenses/>.*/

#include "ui_context.hpp"

#include <float.h>
#include <stdio.h>

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include "controller.hpp"

constexpr static auto footer_note_timeout { 3_s };
constexpr static usize GM1_group_size { 8u };
constexpr static nlc::string_view GM1_group_names[] = { "Piano",         "Chromatic Percussion",
                                                        "Organ",         "Guitar",
                                                        "Bass",          "Strings",
                                                        "Ensemble",      "Brass",
                                                        "Reed",          "Pipe",
                                                        "Synth Lead",    "Synth Pad",
                                                        "Synth Effects", "Ethnic",
                                                        "Percussive",    "Sound Effects" };

static ImVec4 const white { 1.0f, 1.0f, 1.0f, 1.0f };
static ImVec4 const red { 0.8f, 0.0f, 0.0f, 1.0f };

struct PresetGroup final {
    nlc::string_view name;
    nlc::span<Preset const> presets;
};

// True if the bank has preset ids conforming to General MIDI Level 1 Sound Set
// See https://www.midi.org/specifications-old/item/gm-level-1-sound-set
static auto does_bank_match_GM1(Bank const & bank) -> bool {
    if (bank.presets.size() != 128u)
        return false;

    for (auto i = 0; auto const & preset : bank.presets) {
        defer { ++i; };

        if (preset.id != i)
            return false;
    }

    return true;
}

static auto split_presets_in_groups(nlc::allocator & allocator, Bank const & bank)
    -> nlc::vector<PresetGroup> {
    nlc::vector<PresetGroup> res { allocator };

    if (does_bank_match_GM1(bank)) {
        res.reserve(nlc::meta::array_len<decltype(GM1_group_names)>);

        auto span_begin { bank.presets.begin() };
        for (auto const name : GM1_group_names) {
            nlc::span<Preset const> const presets { span_begin, span_begin + GM1_group_size };
            res.append_no_grow(name, presets);
            span_begin = presets.end();
        }
    } else
        res.append("default", nlc::make_span(bank.presets));

    return res;
}

static auto draw_header(MidiViewer const & viewer) -> void {
    constexpr nlc::string_view device_connected_msg { "Midi device connected" };
    constexpr nlc::string_view no_device_connected_msg { "No midi device connected" };

    auto const has_connected_device { viewer.has_connected_device() };
    auto const color { has_connected_device ? white : red };

    ImGui::TextColored(color,
                       "%s",
                       has_connected_device ? device_connected_msg.ptr()
                                            : no_device_connected_msg.ptr());

    auto const current_preset = viewer.get_current_preset();
    ImGui::Text("Current instrument: '%d - %s' from bank %d",
                current_preset.id.preset_id,
                current_preset.preset_name.ptr(),
                current_preset.id.bank_id);
}

static auto draw_presets(nlc::allocator & allocator,
                         MidiController const & controller,
                         int const bank_id,
                         nlc::span<Preset const> const presets,
                         int & selected_bank_id,
                         int & selected_preset_id) -> nlc::optional<FooterNote> {
    auto const is_bank_selected { selected_bank_id == bank_id };
    nlc::optional<FooterNote> footer_msg;

    for (auto const & preset : presets) {
        auto const is_preset_selected { selected_preset_id == preset.id };
        auto const is_selected { is_bank_selected && is_preset_selected };

        constexpr usize buffer_size { 128u };
        char preset_name_buffer[buffer_size];
        auto const written_bytes { snprintf(preset_name_buffer, buffer_size, "%d - ", preset.id) };
        nlc_assert_msg(written_bytes > 0,
                       "couldn't format preset name",
                       nlc_dump_var(preset.id),
                       nlc_dump_var(preset.name));
        nlc::make_null_terminated(preset.name,
                                  preset_name_buffer + written_bytes,
                                  buffer_size - static_cast<usize>(written_bytes));

        if (ImGui::Selectable(preset_name_buffer, is_selected) && is_selected == false) {
            nlc::string_stream sstr { allocator };

            if (controller.select_preset(PresetId { .bank_id = bank_id, .preset_id = preset.id })) {
                selected_bank_id = bank_id;
                selected_preset_id = preset.id;

                sstr << "Success: switched to preset '"
                     << nlc::string_view::from_c_str(preset_name_buffer) << "' from bank " << bank_id;
                footer_msg =
                    FooterNote { .message = nlc::string { allocator, sstr.c_str(), sstr.size() },
                                 .type = FooterNote::Type::Info };
            } else {
                sstr << "Error: couldn't switch to preset '"
                     << nlc::string_view::from_c_str(preset_name_buffer) << "' from bank " << bank_id;

                nlc_assert(footer_msg == nlc::null);
                footer_msg =
                    FooterNote { .message = nlc::string { allocator, sstr.c_str(), sstr.size() },
                                 .type = FooterNote::Type::Error };
            }
        }

        // Set the initial focus when opening the combo (scrolling + keyboard
        // navigation focus)
        if (is_selected)
            ImGui::SetItemDefaultFocus();
    }

    return footer_msg;
}

static auto draw_preset_banks(nlc::allocator & allocator,
                              MidiViewer const & viewer,
                              MidiController const & controller) -> nlc::optional<FooterNote> {
    constexpr usize buffer_size { 64u };
    static int selected_bank_id { 0 };
    static int selected_preset_id { 0 };

    nlc::optional<FooterNote> footer_msg;

    auto const banks { viewer.get_preset_banks() };
    for (auto const & bank : banks) {
        char bank_name_buffer[buffer_size];
        snprintf(bank_name_buffer, buffer_size, "Bank %d", bank.id);

        if (ImGui::CollapsingHeader(bank_name_buffer)) {
            ImGui::Indent();
            defer { ImGui::Unindent(); };

            char list_name_buffer[buffer_size];

            auto const preset_groups { split_presets_in_groups(allocator, bank) };
            nlc::optional<FooterNote> preset_group_msg;
            if (preset_groups.size() <= 1u) {
                snprintf(list_name_buffer, buffer_size, "##bank_%d", bank.id);

                preset_group_msg = draw_presets(allocator,
                                                controller,
                                                bank.id,
                                                preset_groups.front().presets,
                                                selected_bank_id,
                                                selected_preset_id);
            } else {
                for (auto const & group : preset_groups) {
                    snprintf(list_name_buffer,
                             buffer_size,
                             "##bank_%d_group_%s",
                             bank.id,
                             group.name.ptr());

                    if (ImGui::TreeNode(list_name_buffer, "%s", group.name.ptr())) {
                        defer { ImGui::TreePop(); };
                        preset_group_msg = draw_presets(allocator,
                                                        controller,
                                                        bank.id,
                                                        group.presets,
                                                        selected_bank_id,
                                                        selected_preset_id);
                    }
                }
            }

            if (preset_group_msg != nlc::null) {
                nlc_assert_msg(footer_msg == nlc::null, "there should only by on message by frame");
                footer_msg = preset_group_msg.extract();
            }
        }
    }

    return footer_msg;
}

static auto draw_footer(FooterNote const & note, float const footer_size) -> void {
    auto const color = [&note] {
        switch (note.type) {
            case FooterNote::Type::Info: return white;
            case FooterNote::Type::Error: return red;
        }

        unreachable;
    }();

    ImGui::SetCursorPosY(ImGui::GetIO().DisplaySize.y - footer_size);
    ImGui::Separator();
    ImGui::TextColored(color, "%s", note.message.c_str());
}

UIContext::UIContext(GLFWwindow & window) {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(&window, true);
    ImGui_ImplOpenGL3_Init("#version 300 es");

    // Init for first frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

UIContext::~UIContext() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

auto UIContext::update(MidiViewer const & viewer,
                       MidiController const & controller,
                       DeviceConnexionEvent const device_connexion_event) -> void {
    ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
    ImGui::SetNextWindowSize(ImGui::GetIO().DisplaySize);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    defer { ImGui::PopStyleVar(); };

    {
        auto const footer_size { ImGui::GetTextLineHeightWithSpacing() +
                                 ImGui::GetStyle().ItemSpacing.y };

        ImGui::Begin("main window", nullptr, ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoResize);
        defer { ImGui::End(); };

        draw_header(viewer);

        {
            ImGui::BeginGroup();
            defer { ImGui::EndGroup(); };

            constexpr ImGuiWindowFlags child_flags { 0 };
            constexpr bool has_border { false };
            const bool is_child_visible {
                ImGui::BeginChild("##presets",
                                  ImVec2(ImGui::GetContentRegionAvail().x,
                                         ImGui::GetContentRegionAvail().y - footer_size +
                                             ImGui::GetStyle().ItemSpacing.y),
                                  has_border,
                                  child_flags)
            };
            defer { ImGui::EndChild(); };

            if (is_child_visible) {
                auto presets_msg = draw_preset_banks(allocator, viewer, controller);
                if (presets_msg != nlc::null)
                    set_footer_message(presets_msg.extract());
            }
        }

        auto device_connexion_msg { get_device_connexion_event_message(device_connexion_event) };
        if (device_connexion_msg != nlc::null)
            set_footer_message(device_connexion_msg.extract());

        if (nlc::time::now() >= footer_msg_hide_time) {
            footer_msg.message = { allocator, nullptr, 0u };
            footer_msg_hide_time = never;
        }

        draw_footer(footer_msg, footer_size);
    }
}

auto UIContext::get_device_connexion_event_message(DeviceConnexionEvent const device_connexion_event)
    -> nlc::optional<FooterNote> {
    switch (device_connexion_event) {
        case DeviceConnexionEvent::device_connected: {
            constexpr nlc::string_view message { "Success: connected to MIDI device" };
            return FooterNote { .message = { allocator, message }, .type = FooterNote::Type::Info };
        }
        case DeviceConnexionEvent::device_disconnected: {
            constexpr nlc::string_view message { "Error: MIDI device has been disconnected" };
            return FooterNote { .message = { allocator, message }, .type = FooterNote::Type::Error };
        }
        case DeviceConnexionEvent::no_event: [[fallthrough]];
        default: return nlc::null;
    }
}

auto UIContext::set_footer_message(FooterNote message) -> void {
    footer_msg = nlc::move(message);
    footer_msg_hide_time = nlc::time::now() + footer_note_timeout;
}
