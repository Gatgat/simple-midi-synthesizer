# Simple Midi Synthesizer (SMS)

SMS is a very simple project that has a single goal: provide a simple program reading MIDI input from an external device and play it on the default audio output. It provides a basic selection of instrument presets.

![SMS scrrenshot](doc/screenshot.png)

It's a *"weekend project"* so I don't intend to develop it further for now.

## Features

SMS provides:
- detection of MIDI device connexion / deconnexion
- realtime rendering of MIDI stream to default audio output
- choice amongst basic instruments from [General Midi Sound Set Level 1](https://www.midi.org/specifications-old/item/gm-level-1-sound-set)

SMS **does not** provide:
- MIDI channels management
- multiple MIDI devices management
- multiple or dynamic sound font files loading
- audio output selection

If you only want to use it, you can download one of the archives containing a portable version: [sms.tar](https://framagit.org/Gatgat/simple-midi-synthesizer/-/raw/master/archives/sms-1.0.tar) (40.5 Mo) or [sms.zip](https://framagit.org/Gatgat/simple-midi-synthesizer/-/raw/master/archives/sms-1.0.zip) (31.8 Mo).

## How to build

Please note that I have only built the project on Windows 10 so if you have another OS you're on your own.

### Requirements

SMS is built over [FluidSynth](https://github.com/FluidSynth/fluidsynth). Pre-built binaries of its API library are available in [external/fluidsynth](https://framagit.org/Gatgat/simple-midi-synthesizer/-/tree/master/external/fluidsynth), but it still requires to link against some dependencies. so the meson script assumes that you have the following pkg-config files on your machine:
- bzip2.pc
- glib-2.0.pc
- gthread-2.0.pc
- libgettext.pc
- libiconv.pc
- libpcre.pc
- libpcre16.pc
- libpcre32.pc
- libpcreposix.pc
- pcre.pc
- zlib.pc

**Note:** To build FluidSynth on your own, you'll need to build `glib-2.0`. It is [available on conan-center](https://conan.io/center/glib), so what I did was to build this part from conan, then build FluidSynth with its `cmake` script. It was not a pleasure cruise but it's doable and I didn't want to spend more time on the building pipe. Any MR on this part would be appreciated.

### Build

The build pipeline is base on a [meson](https://mesonbuild.com/) script. It uses `pkg-config` to fetch FluidSynth and its dependencies.

Let:
- `<sms_dir>` be the path where you cloned the repository
- `<external_pc_dir>` be the path where you have stored the required `*.pc` files.
- `<build_dir>` be the path where you want to build the project

The commands to configure & build the executable would be

```
meson <build_dir> -Dpkg_config_path=<sms_dir>/external/fluidsynth/lib/pkgconfig:<external_pc_dir>
ninja -C <build_dir> simple_midi_synthesizer.exe
```

### Installation

Once this is done, you can install the program wherever you want (let's say `<sms_install_dir>`) with this command
```
meson install -C <build_dir> --skip-subprojects --destdir <sms_install_dir>
```

Note that `<sms_install_dir>` is relative to `<build_dir>` and not to your current working directory.
